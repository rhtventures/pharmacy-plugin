<?php
 /*
 Plugin Name: Plujo Pharmacy
 Plugin URI: http://plujopress.com
 description: A plugin for Pharmacies to access Plujo features
 Version: 0.5
 Author: Plujo
 Author URI: https://plujo.com
 License: GPL2
 */

//*****************************************************************************************************
//General plugin setup

//Remove "Posts" from admin menu
add_action('admin_menu', function() { remove_menu_page('edit.php'); });


//*****************************************************************************************************
//Global variables
include_once( 'plujo-settings.php' );


//*****************************************************************************************************
//Custom post types
include_once( 'cpt.php' );

