<?php
//*****************************************************************************************************
// Register custom post type
function cpt_event() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'cpt_event' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'cpt_event' ),
		'menu_name'             => __( 'Events', 'cpt_event' ),
		'name_admin_bar'        => __( 'Events', 'cpt_event' ),
		'archives'              => __( 'Events', 'cpt_event' ),
		'attributes'            => __( 'Item Attributes', 'cpt_event' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_event' ),
		'all_items'             => __( 'All Events', 'cpt_event' ),
		'add_new_item'          => __( 'Add New Event', 'cpt_event' ),
		'add_new'               => __( 'Add Event', 'cpt_event' ),
		'new_item'              => __( 'New Event', 'cpt_event' ),
		'edit_item'             => __( 'Edit Event', 'cpt_event' ),
		'update_item'           => __( 'Update Event', 'cpt_event' ),
		'view_item'             => __( 'View Event', 'cpt_event' ),
		'view_items'            => __( 'View Events', 'cpt_event' ),
		'search_items'          => __( 'Search Events', 'cpt_event' ),
		'not_found'             => __( 'Not found', 'cpt_event' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_event' ),
		'featured_image'        => __( 'Featured Image', 'cpt_event' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_event' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_event' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_event' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_event' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_event' ),
		'items_list'            => __( 'Items list', 'cpt_event' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_event' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_event' ),
	);
	$rewrite = array(
		'slug'                  => 'events',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Event', 'cpt_event' ),
		'description'           => __( 'Upcoming and historical calendar of events', 'cpt_event' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
    'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'event',
    'archive_seo_title'     => 'Events',
    'archive_meta_desc'     => ''
	);
	register_post_type( 'cpt_event', $args );

}
add_action( 'init', 'cpt_event', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function event_taxonomy() {

    register_taxonomy(
        'event_categories',
        'cpt_event',
        array(
            'label' => __( 'Event Categories' ),
            'show_admin_column' => true,
            'rewrite' => array( 'slug' => 'event-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'event_taxonomy' );


//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function event_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'event_acf_group',
		'title' => 'Event Settings',
		'fields' => array (
      array (
				'key' => 'event_description',
				'label' => 'Description',
				'name' => 'event_description',
				'type' => 'wysiwyg',
			),
      array (
				'key' => 'event_date_start',
				'label' => 'Date Start',
				'name' => 'event_date_start',
				'type' => 'date_picker',
			),
      array (
				'key' => 'event_date_end',
				'label' => 'Date End',
				'name' => 'event_date_end',
				'type' => 'date_picker',
			),      
      array (
				'key' => 'event_location',
				'label' => 'Location',
				'name' => 'event_location',
				'type' => 'text',
			),
      array (
				'key' => 'event_image',
				'label' => 'Image',
				'name' => 'event_image',
				'type' => 'image',
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_event',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'event_my_acf_add_local_field_groups');

function event_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'event_image'=>'Image', 'event_location'=>'Location','event_date_start'=>'Date Start','event_date_end'=>'Date End');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_event_posts_columns', 'event_add_acf_columns' );

function event_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'event_image':
       echo '<img src="'.get_field( 'event_image', $post_id )['url'].'" width="100" />';
       break;  
     case 'event_location':
     case 'event_date_start':
     case 'event_date_end':     
       echo get_field( $column, $post_id );
       break; 
   }
}
add_action ( 'manage_cpt_event_posts_custom_column', 'event_custom_column', 10, 2 );


//*****************************************************************************************************
//Order & filter public archive page 
add_action( 'pre_get_posts', 'event_archive_orderby'); 
function event_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_event') ):    
  
          /*//change order*/
          $query->set( 'meta_key', 'event_date_end' );
          $query->set( 'orderby', 'meta_value_num' );
          $query->set( 'order', 'ASC' );  
  
          //filter for only future events
          $meta_query = $query->get('meta_query');

          $meta_query = [];
          $meta_query[] = array(
                  'key'		=> 'event_date_end',
                  'value'		=> date('Y-m-d', strtotime('yesterday') ),
                  'compare'	=> '>',
                  'type' => 'DATE'
              );
          $query->set('meta_query', $meta_query);
          
  
    endif;
};


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function event_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('event_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_event' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'event_taxonomy_filters' ); 

