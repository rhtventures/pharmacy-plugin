<?php
//*****************************************************************************************************
// Register custom post type
function cpt_staff() {

	$labels = array(
		'name'                  => _x( 'Staff', 'Post Type General Name', 'cpt_staff' ),
		'singular_name'         => _x( 'Staff', 'Post Type Singular Name', 'cpt_staff' ),
		'menu_name'             => __( 'Staff', 'cpt_staff' ),
		'name_admin_bar'        => __( 'Staff', 'cpt_staff' ),
		'archives'              => __( 'Staff', 'cpt_staff' ),
		'attributes'            => __( 'Item Attributes', 'cpt_staff' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_staff' ),
		'all_items'             => __( 'All Staff', 'cpt_staff' ),
		'add_new_item'          => __( 'Add New Staff', 'cpt_staff' ),
		'add_new'               => __( 'Add Staff', 'cpt_staff' ),
		'new_item'              => __( 'New Staff', 'cpt_staff' ),
		'edit_item'             => __( 'Edit Staff', 'cpt_staff' ),
		'update_item'           => __( 'Update Staff', 'cpt_staff' ),
		'view_item'             => __( 'View Staff', 'cpt_staff' ),
		'view_items'            => __( 'View Staff', 'cpt_staff' ),
		'search_items'          => __( 'Search Staff', 'cpt_staff' ),
		'not_found'             => __( 'Not found', 'cpt_staff' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_staff' ),
		'featured_image'        => __( 'Featured Image', 'cpt_staff' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_staff' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_staff' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_staff' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_staff' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_staff' ),
		'items_list'            => __( 'Items list', 'cpt_staff' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_staff' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_staff' ),
	);
	$rewrite = array(
		'slug'                  => 'staff',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Staff', 'cpt_staff' ),
		'description'           => __( 'Staff articles and press releases', 'cpt_staff' ),
		'labels'                => $labels,
		'supports'              => array('title'),
    'taxonomies'            => array( 'staff_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
    'show_in_nav_menus'     => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'staff',
	);
	register_post_type( 'cpt_staff', $args );
  
}
add_action( 'init', 'cpt_staff', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function staff_taxonomy() {

    register_taxonomy(
        'staff_categories',
        'cpt_staff',
        array(
            'label' => __( 'Staff Categories' ),
            'show_admin_column' => true,
            'publicly_queryable' => false,
            'rewrite' => array( 'slug' => 'staff-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'staff_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function staff_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'staff_acf_group',
		'title' => 'Staff Settings',
		'fields' => array (
      array (
				'key' => 'staff_fname',
				'label' => 'First Name',
				'name' => 'staff_fname',
				'type' => 'text',
			),   
      array (
				'key' => 'staff_lname',
				'label' => 'Last Name',
				'name' => 'staff_lname',
				'type' => 'text',
			),        
      array (
				'key' => 'staff_title',
				'label' => 'Title',
				'name' => 'staff_title',
				'type' => 'text',
			),      
      array (
				'key' => 'staff_description',
				'label' => 'Description',
				'name' => 'staff_description',
				'type' => 'wysiwyg',
			),
      array (
				'key' => 'staff_image',
				'label' => 'Image',
				'name' => 'staff_image',
				'type' => 'image',
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_staff',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'staff_my_acf_add_local_field_groups');

function staff_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'staff_fname'=>'First Name', 'staff_lname'=>'Last Name', 'staff_image'=>'Image' );
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_staff_posts_columns', 'staff_add_acf_columns' );

function staff_custom_column ( $column, $post_id ) {
   switch ( $column ) {       
     case 'staff_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';
       break;  
     case 'staff_fname':
     case 'staff_lname':   
       echo get_field( $column, $post_id );
       break;         
     case 'staff_description':
       echo wp_trim_words( get_field( $column, $post_id ), 10 );
       break;        
   }
}
add_action ( 'manage_cpt_staff_posts_custom_column', 'staff_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function staff_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('staff_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_staff' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'staff_taxonomy_filters' );  


//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'staff_archive_orderby'); 
function staff_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_staff') ):              
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' );
          $query->set( 'posts_per_page', 30 );
    endif;    
};