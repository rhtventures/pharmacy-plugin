<?php
//*****************************************************************************************************
// Register custom post type
function cpt_blog() {

	$labels = array(
		'name'                  => _x( 'Blog', 'Post Type General Name', 'cpt_blog' ),
		'singular_name'         => _x( 'Blog', 'Post Type Singular Name', 'cpt_blog' ),
		'menu_name'             => __( 'Blogs', 'cpt_blog' ),
		'name_admin_bar'        => __( 'Blog', 'cpt_blog' ),
		'archives'              => __( 'Blogs', 'cpt_blog' ),
		'attributes'            => __( 'Item Attributes', 'cpt_blog' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_blog' ),
		'all_items'             => __( 'All Blogs', 'cpt_blog' ),
		'add_new_item'          => __( 'Add New Blog', 'cpt_blog' ),
		'add_new'               => __( 'Add Blog', 'cpt_blog' ),
		'new_item'              => __( 'New Blog', 'cpt_blog' ),
		'edit_item'             => __( 'Edit Blog', 'cpt_blog' ),
		'update_item'           => __( 'Update Blog', 'cpt_blog' ),
		'view_item'             => __( 'View Blog', 'cpt_blog' ),
		'view_items'            => __( 'View Blog', 'cpt_blog' ),
		'search_items'          => __( 'Search Blog', 'cpt_blog' ),
		'not_found'             => __( 'Not found', 'cpt_blog' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_blog' ),
		'featured_image'        => __( 'Featured Image', 'cpt_blog' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_blog' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_blog' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_blog' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_blog' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_blog' ),
		'items_list'            => __( 'Items list', 'cpt_blog' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_blog' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_blog' ),
	);
	$rewrite = array(
		'slug'                  => 'blogs',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Blogs', 'cpt_blog' ),
		'description'           => __( 'Blog articles', 'cpt_blog' ),
		'labels'                => $labels,
		'supports'              => array('title'),
    'taxonomies'            => array( 'blog_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-media-document',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'blog',
	);
	register_post_type( 'cpt_blog', $args );
  
}
add_action( 'init', 'cpt_blog', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function blog_taxonomy() {

    register_taxonomy(
        'blog_categories',
        'cpt_blog',
        array(
            'label' => __( 'Blog Categories' ),
            'show_admin_column' => true,            
            'rewrite' => array( 'slug' => 'blog-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'blog_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function blog_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'blog_acf_group',
		'title' => 'Blog Settings',
		'fields' => array (
      array (
				'key' => 'blog_article',
				'label' => 'Article',
				'name' => 'blog_article',
				'type' => 'wysiwyg',
			),      
      array (
				'key' => 'blog_image',
				'label' => 'Image',
				'name' => 'blog_image',
				'type' => 'image',
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_blog',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'blog_my_acf_add_local_field_groups');

function blog_add_acf_columns ( $columns ) {    
  $custom_columns = array(  'blog_image'=>'Image' );
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_blog_posts_columns', 'blog_add_acf_columns' );

function blog_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'blog_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';
       break;             
   }
}
add_action ( 'manage_cpt_blog_posts_custom_column', 'blog_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function blog_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('blog_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_blog' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'blog_taxonomy_filters' );  