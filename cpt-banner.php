<?php
//*****************************************************************************************************
// Register custom post type
function cpt_banner() {

	$labels = array(
		'name'                  => _x( 'Banners', 'Post Type General Name', 'cpt_banner' ),
		'singular_name'         => _x( 'Banner', 'Post Type Singular Name', 'cpt_banner' ),
		'menu_name'             => __( 'Banners', 'cpt_banner' ),
		'name_admin_bar'        => __( 'Banners', 'cpt_banner' ),
		'archives'              => __( 'Banners', 'cpt_banner' ),
		'attributes'            => __( 'Item Attributes', 'cpt_banner' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_banner' ),
		'all_items'             => __( 'All Banners', 'cpt_banner' ),
		'add_new_item'          => __( 'Add New Banner', 'cpt_banner' ),
		'add_new'               => __( 'Add Banner', 'cpt_banner' ),
		'new_item'              => __( 'New Banner', 'cpt_banner' ),
		'edit_item'             => __( 'Edit Banner', 'cpt_banner' ),
		'update_item'           => __( 'Update Banner', 'cpt_banner' ),
		'view_item'             => __( 'View Banner', 'cpt_banner' ),
		'view_items'            => __( 'View Banners', 'cpt_banner' ),
		'search_items'          => __( 'Search Banners', 'cpt_banner' ),
		'not_found'             => __( 'Not found', 'cpt_banner' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_banner' ),
		'featured_image'        => __( 'Featured Image', 'cpt_banner' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_banner' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_banner' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_banner' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_banner' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_banner' ),
		'items_list'            => __( 'Items list', 'cpt_banner' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_banner' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_banner' ),
	);
	$rewrite = array(
		'slug'                  => 'banner',
		'with_front'            => false,
		'pages'                 => false,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => __( 'Banner', 'cpt_banner' ),
		'description'           => __( 'Banner, advertisements and small visual blocks', 'cpt_banner' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
    'taxonomies'            => array( 'banner_locations' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-images-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'banner',
	);
	register_post_type( 'cpt_banner', $args );

}
add_action( 'init', 'cpt_banner', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function banner_taxonomy() {

    register_taxonomy(
        'banner_locations',
        'cpt_banner',
        array(
            'label' => __( 'Locations' ),
            'show_admin_column' => true,
            'publicly_queryable' => false,
            'rewrite' => array( 'slug' => 'banner-locations' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'banner_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function banner_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'banner_acf_group',
		'title' => 'Banner Settings',
		'fields' => array (
      array (
				'key' => 'banner_link',
				'label' => 'Link',
				'name' => 'banner_link',
				'type' => 'link',
			),
      array (
				'key' => 'banner_image',
				'label' => 'Image',
				'name' => 'banner_image',
				'type' => 'image',
			),
      array (
				'key' => 'banner_blocks',
				'label' => 'Blocks',
				'name' => 'banner_blocks',
				'type' => 'select',
        'choices' => array(
          '1'	  => '1 Block',
          '2'	  => '2 Blocks',
          '3'	  => '3 Blocks',
          '4'   => '4 Blocks'
          )
        )   
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_banner',
				),
			),
		),
	));
	
}
add_action('acf/init', 'banner_my_acf_add_local_field_groups');

//*****************************************************************************************************
//add fields to admin list

function banner_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'banner_image'=>'Image', 'banner_blocks'=>'Blocks', 'banner_link'=>'Link');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_banner_posts_columns', 'banner_add_acf_columns' );

function banner_custom_column ( $column, $post_id ) {
   switch ( $column ) {            
     case 'banner_blocks':
       echo get_field( $column, $post_id );
       break;         
     case 'banner_image':
       echo '<img src="'.get_field( 'banner_image', $post_id )['url'].'" width="100" />';
       break;  
     case 'banner_link':
       $link = get_field( 'banner_link', $post_id );
       echo ( isset( $link['url'] ) )? $link['url'] : '' ;
       break;      
   }
}
add_action ( 'manage_cpt_banner_posts_custom_column', 'banner_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function banner_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('banner_locations');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_banner' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'banner_taxonomy_filters' );  


//*****************************************************************************************************
//Add ability to sort by column

add_filter( 'manage_edit-cpt_banner_sortable_columns', 'banner_admin_sortable_columns' );
function banner_admin_sortable_columns( $columns ) {
  $columns['banner_blocks'] = 'banner_blocks';
  return $columns;
}

add_action( 'pre_get_posts', 'banner_custom_orderby' );
function banner_custom_orderby( $query ) {
  if ( ! is_admin() )
    return;

  $orderby = $query->get( 'orderby');
  

  if ( 'banner_blocks' == $orderby ) {
    $query->set( 'meta_key', 'banner_blocks' );
    $query->set( 'orderby', 'meta_value_num' );
  }
  
  
}