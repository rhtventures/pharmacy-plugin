<?php
//*****************************************************************************************************
// Register custom post type
function cpt_product() {

	$labels = array(
		'name'                  => _x( 'Products', 'Post Type General Name', 'cpt_product' ),
		'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'cpt_product' ),
		'menu_name'             => __( 'Products', 'cpt_product' ),
		'name_admin_bar'        => __( 'Products', 'cpt_product' ),
		'archives'              => __( 'Product', 'cpt_product' ),
		'attributes'            => __( 'Item Attributes', 'cpt_product' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_product' ),
		'all_items'             => __( 'All Product', 'cpt_product' ),
		'add_new_item'          => __( 'Add New Product', 'cpt_product' ),
		'add_new'               => __( 'Add Product', 'cpt_product' ),
		'new_item'              => __( 'New Product', 'cpt_product' ),
		'edit_item'             => __( 'Edit Product', 'cpt_product' ),
		'update_item'           => __( 'Update Product', 'cpt_product' ),
		'view_item'             => __( 'View Product', 'cpt_product' ),
		'view_items'            => __( 'View Products', 'cpt_product' ),
		'search_items'          => __( 'Search Product', 'cpt_product' ),
		'not_found'             => __( 'Not found', 'cpt_product' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_product' ),
		'featured_image'        => __( 'Featured Image', 'cpt_product' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_product' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_product' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_product' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_product' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_product' ),
		'items_list'            => __( 'Items list', 'cpt_product' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_product' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_product' ),
	);
	$rewrite = array(
		'slug'                  => 'products',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Products', 'cpt_product' ),
		'description'           => __( 'Product articles and press releases', 'cpt_product' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
    'taxonomies'            => array( 'product_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-cart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'product',
	);
	register_post_type( 'cpt_product', $args );

}
add_action( 'init', 'cpt_product', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function product_taxonomy() {

    register_taxonomy(
        'product_categories',
        'cpt_product',
        array(
            'label' => __( 'Product Categories' ),
            'show_admin_column' => true,
            'rewrite' => array( 'slug' => 'product-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'product_taxonomy' );

//*****************************************************************************************************
//Add custom fields to categories
function product_category_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'product_category_acf_group',
		'title' => 'Product Category Settings',
		'fields' => array (
      array (
				'key' => 'product_category_image',
				'label' => 'Image',
				'name' => 'product_category_image',
				'type' => 'image',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'product_categories',
				),
			),
		),
	));
	
}
add_action('acf/init', 'product_category_my_acf_add_local_field_groups');



//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function product_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'product_acf_group',
		'title' => 'Product Settings',
		'fields' => array (
      array (
				'key' => 'product_description',
				'label' => 'Description',
				'name' => 'product_description',
				'type' => 'wysiwyg',
			),
      array (
				'key'   => 'featured',
				'label' => 'Featured',
				'name'  => 'featured',
				'type'  => 'true_false'
			),
      array (
				'key' => 'product_price',
				'label' => 'Price',
				'name' => 'product_price',
				'type' => 'text',
			),    
      array (
				'key' => 'product_image',
				'label' => 'Image',
				'name' => 'product_image',
				'type' => 'image',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_product',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'product_my_acf_add_local_field_groups');

function product_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'product_image'=>'Image', 'product_price'=>'Price', 'product_description'=>'Description', 'featured'=>'Featured');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_product_posts_columns', 'product_add_acf_columns' );

function product_custom_column ( $column, $post_id ) {
   switch ( $column ) {     
     case 'featured':       
       echo (get_field( $column, $post_id ) )? 'Yes' : 'No';
       break;               
     case 'product_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';
       break;  
     case 'product_price':
       echo get_field( $column, $post_id );
       break;     
     case 'product_description':
       echo wp_trim_words( get_field( $column, $post_id ), 10 );
       break;        
   }
}
add_action ( 'manage_cpt_product_posts_custom_column', 'product_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function product_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('product_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_product' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'product_taxonomy_filters' );  


//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'product_archive_orderby'); 
function product_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_product') ):              
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' );
    endif;
};
