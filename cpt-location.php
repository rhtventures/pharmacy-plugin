<?php
//*****************************************************************************************************
// Register custom post type
function cpt_location() {

	$labels = array(
		'name'                  => _x( 'Location', 'Post Type General Name', 'cpt_location' ),
		'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'cpt_location' ),
		'menu_name'             => __( 'Locations', 'cpt_location' ),
		'name_admin_bar'        => __( 'Location', 'cpt_location' ),
		'archives'              => __( 'Locations', 'cpt_location' ),
		'attributes'            => __( 'Item Attributes', 'cpt_location' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_location' ),
		'all_items'             => __( 'All Locations', 'cpt_location' ),
		'add_new_item'          => __( 'Add New Location', 'cpt_location' ),
		'add_new'               => __( 'Add Location', 'cpt_location' ),
		'new_item'              => __( 'New Location', 'cpt_location' ),
		'edit_item'             => __( 'Edit Location', 'cpt_location' ),
		'update_item'           => __( 'Update Location', 'cpt_location' ),
		'view_item'             => __( 'View Location', 'cpt_location' ),
		'view_items'            => __( 'View Location', 'cpt_location' ),
		'search_items'          => __( 'Search Location', 'cpt_location' ),
		'not_found'             => __( 'Not found', 'cpt_location' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_location' ),
		'featured_image'        => __( 'Featured Image', 'cpt_location' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_location' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_location' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_location' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_location' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_location' ),
		'items_list'            => __( 'Items list', 'cpt_location' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_location' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_location' ),
	);
	$rewrite = array(
		'slug'                  => 'locations',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Locations', 'cpt_location' ),
		'description'           => __( 'Location articles', 'cpt_location' ),
		'labels'                => $labels,
		'supports'              => array('title'),
    'taxonomies'            => array( 'location_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-pressthis',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'location',
	);
	register_post_type( 'cpt_location', $args );
  
}
add_action( 'init', 'cpt_location', 0 );


//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function location_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'location_acf_group',
		'title' => 'Location Settings',
		'fields' => array (
      array (
				'key' => 'location_name',
				'label' => 'Name',
				'name' => 'location_name',
				'type' => 'text',
			),    
      array (
				'key' => 'location_store_id',
				'label' => 'Store Id',
				'name' => 'location_store_id',
				'type' => 'text',
			),       
      array (
				'key' => 'location_address_street',
				'label' => 'Street',
				'name' => 'location_address_street',
				'type' => 'text',
			),
      array (
				'key' => 'location_address_city',
				'label' => 'City',
				'name' => 'location_address_city',
				'type' => 'text',
			),
      array (
				'key' => 'location_address_province',
				'label' => 'Province',
				'name' => 'location_address_province',
				'type' => 'select',
        'choices' => array(
          'Alberta'	                  => 'Alberta',
          'British Columbia'	        => 'British Columbia',
          'Manitoba'	                => 'Manitoba',
          'New Brunswick'             => 'New Brunswick',
          'Newfoundland & Labrador'   => 'Newfoundland & Labrador',
          'Nova Scotia'               => 'Nova Scotia',
          'Ontario'                   => 'Nova Scotia',
          'Ontario'                   => 'Ontario',
          'Prince Edward Island'      => 'Prince Edward Island',
          'Quebec'                    => 'Quebec',
          'Saskatchewan'              => 'Saskatchewan',
          'Northwest Territories'     => 'Northwest Territories',
          'Nunavut'                   => 'Nunavut',
          'Yukon'                     => 'Yukon'          
          )
        ),      
      array (
				'key' => 'location_address_postal',
				'label' => 'Postal',
				'name' => 'location_address_postal',
				'type' => 'text',
			),      
      array (
				'key' => 'location_phone_number',
				'label' => 'Phone Number',
				'name' => 'location_phone_number',
				'type' => 'text',
			),        
      array (
				'key' => 'location_fax_number',
				'label' => 'Fax Number',
				'name' => 'location_fax_number',
				'type' => 'text',
			),        
      array (
				'key' => 'location_description',
				'label' => 'Description',
				'name' => 'location_description',
				'type' => 'wysiwyg',
			),     
      array (
				'key' => 'location_hours',
				'label' => 'Hours',
				'name' => 'location_hours',
				'type' => 'wysiwyg',
			),      
      array (
				'key' => 'location_contact_form',
				'label' => 'Contact Form',
				'name' => 'location_contact_form',
				'type' => 'wysiwyg',
			),       
      array (
				'key' => 'location_image',
				'label' => 'Image',
				'name' => 'location_image',
				'type' => 'image',
			),
      array (
				'key' => 'location_rxrefill_url',
				'label' => 'Rx Refill URL',
				'name' => 'location_rxrefill_url',
				'type' => 'link',
			),            
      array (
				'key' => 'location_googlemb_api',
				'label' => 'Google My Business API Key',
				'name' => 'location_googlemb_api',
				'type' => 'text',
			), 
      array (
				'key' => 'location_googlemb_location_id',
				'label' => 'Google My Business Location ID',
				'name' => 'location_googlemb_location_id',
				'type' => 'text',
			), 
      array (
				'key' => 'location_omnisys_storeid',
				'label' => 'Omnisys Store Id',
				'name' => 'location_omnisys_storeid',
				'type' => 'text',
			),  
      array (
				'key' => 'location_google_reviews',
				'label' => 'Google Reviews',
				'name' => 'location_google_reviews',
				'type' => 'true_false',
        'default_value' => 'true',
			),      
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_location',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'location_my_acf_add_local_field_groups');

function location_add_acf_columns ( $columns ) {    
  $custom_columns = array(  'location_name'=>'Name', 'location_address_street'=>'Street', 'location_address_city'=>'City' );
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_location_posts_columns', 'location_add_acf_columns' );

function location_custom_column ( $column, $post_id ) {
   switch ( $column ) {
      case 'location_address_street':     
      case 'location_address_city':
      case 'location_name':
         echo get_field( $column, $post_id );
         break;
   }
}
add_action ( 'manage_cpt_location_posts_custom_column', 'location_custom_column', 10, 2 );


//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'location_archive_orderby'); 
function location_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_location') ):              
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' );
    endif;    
};
