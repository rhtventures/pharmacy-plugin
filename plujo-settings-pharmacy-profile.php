<?php
$this->plujo_global['fields_array'][$settings_page] = array(
			array(
				'label' => 'Store Name (Full)',
				'id' => 'pharmacy_store_name_full',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Full version of public name of pharmacy store',
				'placeholder' => 'Store Name (Full)',
			),
			array(
				'label' => 'Store Name (Short)',
				'id' => 'pharmacy_store_name_short',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Shortened version of public name of pharmacy store',
				'placeholder' => 'Store Name (Short)',
			),
			array(
				'label' => 'Store Tag Line',
				'id' => 'pharmacy_tag_line',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Marketing tag line for pharmacy',
				'placeholder' => 'Store Tag Line',
			),       
			array(
				'label' => 'Email Address',
				'id' => 'pharmacy_email',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Store email address',
				'placeholder' => 'Email Address',
			),
      array (
				'label' => 'Brand',
        'id' => 'pharmacy_brand',
				'type' => 'select',
        'section' => $settings_page,
        'desc' => 'Store brand',
				'placeholder' => 'Brand',
        'options' => array(
          'pharmasave'    => 'Pharmasave',
          'ida'           => 'IDA'      
          )
        )  
		);