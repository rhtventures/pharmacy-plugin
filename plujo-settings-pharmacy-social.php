<?php
$this->plujo_global['fields_array'][$settings_page] = array(
			array(
				'label' => 'Facebook URL',
				'id' => 'pharmacy_facebook_url',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Full URL to Facebook page',
				'placeholder' => 'Facebook URL',
			),
			array(
				'label' => 'Twitter URL',
				'id' => 'pharmacy_twitter_url',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Full URL to Twitter page',
				'placeholder' => 'Twitter URL',
			),
			array(
				'label' => 'LinkedIn URL',
				'id' => 'pharmacy_linkedin_url',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Full URL to LinkedIn page',
				'placeholder' => 'LinkedIn URL',
			),
			array(
				'label' => 'Instagram URL',
				'id' => 'pharmacy_instagram_url',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Full URL to Instagram page',
				'placeholder' => 'Instagram URL',
			),       
			array(
				'label' => 'YouTube URL',
				'id' => 'pharmacy_youtube_url',
				'type' => 'text',
				'section' => $settings_page,
				'desc' => 'Full URL to YouTube page',
				'placeholder' => 'YouTube URL',
			)
		);