<?php
//*****************************************************************************************************
// Register custom post type
function cpt_testimonial() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'cpt_testimonial' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'cpt_testimonial' ),
		'menu_name'             => __( 'Testimonials', 'cpt_testimonial' ),
		'name_admin_bar'        => __( 'Testimonial', 'cpt_testimonial' ),
		'archives'              => __( 'Testimonials', 'cpt_testimonial' ),
		'attributes'            => __( 'Item Attributes', 'cpt_testimonial' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_testimonial' ),
		'all_items'             => __( 'All Testimonials', 'cpt_testimonial' ),
		'add_new_item'          => __( 'Add New Testimonial', 'cpt_testimonial' ),
		'add_new'               => __( 'Add Testimonial', 'cpt_testimonial' ),
		'new_item'              => __( 'New Testimonial', 'cpt_testimonial' ),
		'edit_item'             => __( 'Edit Testimonial', 'cpt_testimonial' ),
		'update_item'           => __( 'Update Testimonial', 'cpt_testimonial' ),
		'view_item'             => __( 'View Testimonial', 'cpt_testimonial' ),
		'view_items'            => __( 'View Testimonial', 'cpt_testimonial' ),
		'search_items'          => __( 'Search Testimonial', 'cpt_testimonial' ),
		'not_found'             => __( 'Not found', 'cpt_testimonial' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_testimonial' ),
		'featured_image'        => __( 'Featured Image', 'cpt_testimonial' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_testimonial' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_testimonial' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_testimonial' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_testimonial' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_testimonial' ),
		'items_list'            => __( 'Items list', 'cpt_testimonial' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_testimonial' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_testimonial' ),
	);
	$rewrite = array(
		'slug'                  => 'testimonials',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Testimonials', 'cpt_testimonial' ),
		'description'           => __( 'Testimonial articles and press releases', 'cpt_testimonial' ),
		'labels'                => $labels,
		'supports'              => array('title'),
    'taxonomies'            => array( 'testimonial_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-editor-quote',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'testimonial',
	);
	register_post_type( 'cpt_testimonial', $args );
  
}
add_action( 'init', 'cpt_testimonial', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function testimonial_taxonomy() {

    register_taxonomy(
        'testimonial_categories',
        'cpt_testimonial',
        array(
            'label' => __( 'Testimonial Categories' ),
            'show_admin_column' => true,
            'publicly_queryable' => false,
            'rewrite' => array( 'slug' => 'testimonial-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'testimonial_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function testimonial_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'testimonial_acf_group',
		'title' => 'Testimonial Settings',
		'fields' => array (
      array (
				'key' => 'testimonial_fname',
				'label' => 'First Name',
				'name' => 'testimonial_fname',
				'type' => 'text',
			),   
      array (
				'key' => 'testimonial_lname',
				'label' => 'Last Name',
				'name' => 'testimonial_lname',
				'type' => 'text',
			),        
      array (
				'key' => 'testimonial_title',
				'label' => 'Title',
				'name' => 'testimonial_title',
				'type' => 'text',
			),       
      array (
				'key' => 'testimonial_quote',
				'label' => 'Quote',
				'name' => 'testimonial_quote',
				'type' => 'wysiwyg',
			),
      array (
				'key' => 'testimonial_image',
				'label' => 'Image',
				'name' => 'testimonial_image',
				'type' => 'image',
			),
      array (
				'key'   => 'featured',
				'label' => 'Featured',
				'name'  => 'featured',
				'type'  => 'true_false'
			)  
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_testimonial',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'testimonial_my_acf_add_local_field_groups');

function testimonial_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'testimonial_fname'=>'First Name', 'testimonial_lname'=>'Last Name', 'testimonial_image'=>'Image', 'featured'=>'Featured');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_testimonial_posts_columns', 'testimonial_add_acf_columns' );

function testimonial_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'featured':       
       echo (get_field( $column, $post_id ) )? 'Yes' : 'No';
       break;        
     case 'testimonial_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';
       break;  
     case 'testimonial_fname':
     case 'testimonial_lname':
       echo get_field( $column, $post_id );
       break;         
     case 'testimonial_quote':
       echo wp_trim_words( get_field( $column, $post_id ), 20 );
       break;        
   }
}
add_action ( 'manage_cpt_testimonial_posts_custom_column', 'testimonial_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function testimonial_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('testimonial_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_testimonial' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'testimonial_taxonomy_filters' );  


//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'testimonial_archive_orderby'); 
function testimonial_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_testimonial') ):              
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' ); 
    endif;    
};
