<?php
//*****************************************************************************************************
// Register custom post type
function cpt_news() {

	$labels = array(
		'name'                  => _x( 'News', 'Post Type General Name', 'cpt_news' ),
		'singular_name'         => _x( 'News', 'Post Type Singular Name', 'cpt_news' ),
		'menu_name'             => __( 'News', 'cpt_news' ),
		'name_admin_bar'        => __( 'News', 'cpt_news' ),
		'archives'              => __( 'News', 'cpt_news' ),
		'attributes'            => __( 'Item Attributes', 'cpt_news' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_news' ),
		'all_items'             => __( 'All News', 'cpt_news' ),
		'add_new_item'          => __( 'Add New News', 'cpt_news' ),
		'add_new'               => __( 'Add News', 'cpt_news' ),
		'new_item'              => __( 'New News', 'cpt_news' ),
		'edit_item'             => __( 'Edit News', 'cpt_news' ),
		'update_item'           => __( 'Update News', 'cpt_news' ),
		'view_item'             => __( 'View News', 'cpt_news' ),
		'view_items'            => __( 'View Newss', 'cpt_news' ),
		'search_items'          => __( 'Search News', 'cpt_news' ),
		'not_found'             => __( 'Not found', 'cpt_news' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_news' ),
		'featured_image'        => __( 'Featured Image', 'cpt_news' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_news' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_news' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_news' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_news' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_news' ),
		'items_list'            => __( 'Items list', 'cpt_news' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_news' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_news' ),
	);
	$rewrite = array(
		'slug'                  => 'news',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'News', 'cpt_news' ),
		'description'           => __( 'News articles and press releases', 'cpt_news' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
    'taxonomies'            => array( 'news_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-media-text',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'news',
	);
	register_post_type( 'cpt_news', $args );

}
add_action( 'init', 'cpt_news', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function news_taxonomy() {

    register_taxonomy(
        'news_categories',
        'cpt_news',
        array(
            'label' => __( 'News Categories' ),
            'show_admin_column' => true,            
            'rewrite' => array( 'slug' => 'news-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'news_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function news_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'news_acf_group',
		'title' => 'News Settings',
		'fields' => array (
      array (
				'key' => 'news_article',
				'label' => 'Article',
				'name' => 'news_article',
				'type' => 'wysiwyg',
			),      
      array (
				'key' => 'news_image',
				'label' => 'Image',
				'name' => 'news_image',
				'type' => 'image',
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_news',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'news_my_acf_add_local_field_groups');

function news_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'news_image'=>'Image');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_news_posts_columns', 'news_add_acf_columns' );

function news_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'news_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';;
       break;      
   }
}
add_action ( 'manage_cpt_news_posts_custom_column', 'news_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function news_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('news_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_news' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'news_taxonomy_filters' );  