<?php
//*****************************************************************************************************
// Register custom post type
function cpt_gallery() {

	$labels = array(
		'name'                  => _x( 'Galleries', 'Post Type General Name', 'cpt_gallery' ),
		'singular_name'         => _x( 'Gallery', 'Post Type Singular Name', 'cpt_gallery' ),
		'menu_name'             => __( 'Galleries', 'cpt_gallery' ),
		'name_admin_bar'        => __( 'Galleries', 'cpt_gallery' ),
		'archives'              => __( 'Galleries', 'cpt_gallery' ),
		'attributes'            => __( 'Item Attributes', 'cpt_gallery' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_gallery' ),
		'all_items'             => __( 'All Galleries', 'cpt_gallery' ),
		'add_new_item'          => __( 'Add New Gallery', 'cpt_gallery' ),
		'add_new'               => __( 'Add Gallery', 'cpt_gallery' ),
		'new_item'              => __( 'New Gallery', 'cpt_gallery' ),
		'edit_item'             => __( 'Edit Gallery', 'cpt_gallery' ),
		'update_item'           => __( 'Update Gallery', 'cpt_gallery' ),
		'view_item'             => __( 'View Gallery', 'cpt_gallery' ),
		'view_items'            => __( 'View Gallery', 'cpt_gallery' ),
		'search_items'          => __( 'Search Gallery', 'cpt_gallery' ),
		'not_found'             => __( 'Not found', 'cpt_gallery' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_gallery' ),
		'featured_image'        => __( 'Featured Image', 'cpt_gallery' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_gallery' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_gallery' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_gallery' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_gallery' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_gallery' ),
		'items_list'            => __( 'Items list', 'cpt_gallery' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_gallery' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_gallery' ),
	);
	$rewrite = array(
		'slug'                  => 'galleries',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Gallery', 'cpt_gallery' ),
		'description'           => __( 'Photo galleries', 'cpt_gallery' ),
		'labels'                => $labels,
		'supports'              => array('title'),
    'taxonomies'            => array( 'gallery_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-image',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'gallery',
	);
	register_post_type( 'cpt_gallery', $args );
  
}
add_action( 'init', 'cpt_gallery', 0 );


//*****************************************************************************************************
//Add custom taxonomy
function gallery_taxonomy() {

    register_taxonomy(
        'gallery_categories',
        'cpt_gallery',
        array(
            'label' => __( 'Gallery Categories' ),
            'show_admin_column' => true,            
            'rewrite' => array( 'slug' => 'gallery-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'gallery_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function gallery_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'gallery_acf_group',
		'title' => 'Gallery Settings',
		'fields' => array (
      array (
				'key' => 'gallery_summary',
				'label' => 'Summary',
				'name' => 'gallery_summary',
				'type' => 'textarea',
			),
      array (
				'key' => 'gallery_photos',
				'label' => 'Photos',
				'name' => 'gallery_photos',
				'type' => 'gallery',
			),      
      array (
				'key' => 'gallery_image',
				'label' => 'Image',
				'name' => 'gallery_image',
				'type' => 'image',
			),
      array (
				'key'   => 'featured',
				'label' => 'Featured',
				'name'  => 'featured',
				'type'  => 'true_false'
			)  
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_gallery',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'gallery_my_acf_add_local_field_groups');

function gallery_add_acf_columns ( $columns ) {    
  $custom_columns = array(  'gallery_image'=>'Image', 'gallery_summary'=>'Summary', 'featured'=>'Featured' );
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_gallery_posts_columns', 'gallery_add_acf_columns' );

function gallery_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'featured':       
       echo (get_field( $column, $post_id ) )? 'Yes' : 'No';
       break;         
     case 'gallery_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';
       break;       
     case 'gallery_summary':
       echo wp_trim_words( get_field( $column, $post_id ), 20 );
       break;        
   }
}
add_action ( 'manage_cpt_gallery_posts_custom_column', 'gallery_custom_column', 10, 2 );

//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function gallery_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('gallery_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_gallery' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'gallery_taxonomy_filters' ); 



//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'gallery_archive_orderby'); 
function gallery_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_gallery') ):              
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' );  
    endif;
};

