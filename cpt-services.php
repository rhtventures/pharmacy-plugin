<?php
//*****************************************************************************************************
// Register custom post type
function cpt_service() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'cpt_service' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'cpt_service' ),
		'menu_name'             => __( 'Services', 'cpt_service' ),
		'name_admin_bar'        => __( 'Service', 'cpt_service' ),
		'archives'              => __( 'Services', 'cpt_service' ),
		'attributes'            => __( 'Item Attributes', 'cpt_service' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_service' ),
		'all_items'             => __( 'All Services', 'cpt_service' ),
		'add_new_item'          => __( 'Add New Service', 'cpt_service' ),
		'add_new'               => __( 'Add Service', 'cpt_service' ),
		'new_item'              => __( 'New Service', 'cpt_service' ),
		'edit_item'             => __( 'Edit Service', 'cpt_service' ),
		'update_item'           => __( 'Update Service', 'cpt_service' ),
		'view_item'             => __( 'View Service', 'cpt_service' ),
		'view_items'            => __( 'View Service', 'cpt_service' ),
		'search_items'          => __( 'Search Service', 'cpt_service' ),
		'not_found'             => __( 'Not found', 'cpt_service' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_service' ),
		'featured_image'        => __( 'Featured Image', 'cpt_service' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_service' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_service' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_service' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_service' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_service' ),
		'items_list'            => __( 'Items list', 'cpt_service' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_service' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_service' ),
	);
	$rewrite = array(
		'slug'                  => 'services',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Services', 'cpt_service' ),
		'description'           => __( 'Service articles', 'cpt_service' ),
		'labels'                => $labels,
		'supports'              => array('title'),
    'taxonomies'            => array( 'service_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-layout',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'service',
	);
	register_post_type( 'cpt_service', $args );
  
}
add_action( 'init', 'cpt_service', 0 );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function service_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'service_acf_group',
		'title' => 'Service Settings',
		'fields' => array (
      array (
				'key' => 'service_description',
				'label' => 'Description',
				'name' => 'service_description',
				'type' => 'wysiwyg',
			),      
      array (
				'key' => 'service_image',
				'label' => 'Image',
				'name' => 'service_image',
				'type' => 'image',
			),      
      array (
				'key' => 'service_icon',
				'label' => 'Icon',
				'name' => 'service_icon',
				'type' => 'image',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_service',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'service_my_acf_add_local_field_groups');

function service_add_acf_columns ( $columns ) {    
  $custom_columns = array(  'service_image'=>'Image');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}


add_filter ( 'manage_cpt_service_posts_columns', 'service_add_acf_columns' );

function service_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'service_image':
       echo '<img src="'.get_field( $column, $post_id )['url'].'" width="100" />';
       break;             
   }
}
add_action ( 'manage_cpt_service_posts_custom_column', 'service_custom_column', 10, 2 );


//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'service_archive_orderby'); 
function service_archive_orderby($query){
    if( !is_admin() && is_archive() && $query->is_main_query() && is_post_type_archive('cpt_service') ):              
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' );
    endif;
};