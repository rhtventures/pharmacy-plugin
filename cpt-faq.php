<?php
//*****************************************************************************************************
// Register custom post type
function cpt_faq() {

	$labels = array(
		'name'                  => _x( 'FAQs', 'Post Type General Name', 'cpt_faq' ),
		'singular_name'         => _x( 'FAQ', 'Post Type Singular Name', 'cpt_faq' ),
		'menu_name'             => __( 'FAQs', 'cpt_faq' ),
		'name_admin_bar'        => __( 'FAQs', 'cpt_faq' ),
		'archives'              => __( 'FAQs', 'cpt_faq' ),
		'attributes'            => __( 'Item Attributes', 'cpt_faq' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpt_faq' ),
		'all_items'             => __( 'All FAQs', 'cpt_faq' ),
		'add_new_item'          => __( 'Add New FAQ', 'cpt_faq' ),
		'add_new'               => __( 'Add FAQ', 'cpt_faq' ),
		'new_item'              => __( 'New FAQ', 'cpt_faq' ),
		'edit_item'             => __( 'Edit FAQ', 'cpt_faq' ),
		'update_item'           => __( 'Update FAQ', 'cpt_faq' ),
		'view_item'             => __( 'View FAQ', 'cpt_faq' ),
		'view_items'            => __( 'View FAQs', 'cpt_faq' ),
		'search_items'          => __( 'Search FAQs', 'cpt_faq' ),
		'not_found'             => __( 'Not found', 'cpt_faq' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpt_faq' ),
		'featured_image'        => __( 'Featured Image', 'cpt_faq' ),
		'set_featured_image'    => __( 'Set featured image', 'cpt_faq' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpt_faq' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpt_faq' ),
		'insert_into_item'      => __( 'Insert into item', 'cpt_faq' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpt_faq' ),
		'items_list'            => __( 'Items list', 'cpt_faq' ),
		'items_list_navigation' => __( 'Items list navigation', 'cpt_faq' ),
		'filter_items_list'     => __( 'Filter items list', 'cpt_faq' ),
	);
	$rewrite = array(
		'slug'                  => 'faq',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'FAQ', 'cpt_faq' ),
		'description'           => __( 'Frequently asked questions', 'cpt_faq' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
    'taxonomies'            => array( 'faq_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-chat',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
		'rest_base'             => 'faq',
	);
	register_post_type( 'cpt_faq', $args );

}
add_action( 'init', 'cpt_faq', 0 );

//*****************************************************************************************************
//Add custom taxonomy
function faq_taxonomy() {

    register_taxonomy(
        'faq_categories',
        'cpt_faq',
        array(
            'label' => __( 'FAQ Categories' ),
            'show_admin_column' => true,
            'publicly_queryable' => false,
            'rewrite' => array( 'slug' => 'faq-categories' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'faq_taxonomy' );

//*****************************************************************************************************
//Add custom fields (using Advanced Custom Fields API)
function faq_my_acf_add_local_field_groups() {
	
	acf_add_local_field_group(array(
		'key' => 'faq_acf_group',
		'title' => 'FAQ Settings',
		'fields' => array (
      array (
				'key' => 'faq_question',
				'label' => 'Question',
				'name' => 'faq_question',
				'type' => 'wysiwyg',
			),
      array (
				'key' => 'faq_answer',
				'label' => 'Answer',
				'name' => 'faq_answer',
				'type' => 'wysiwyg',
			)      
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'cpt_faq',
				),
			),
		),
	));
	
}

//*****************************************************************************************************
//add fields to admin list
add_action('acf/init', 'faq_my_acf_add_local_field_groups');

function faq_add_acf_columns ( $columns ) {    
  $custom_columns = array( 'faq_question'=>'Question', 'faq_answer'=>'Answer');
  
  return array_merge( array_slice( $columns, 0, 2), $custom_columns, array_slice( $columns, 2));
}
add_filter ( 'manage_cpt_faq_posts_columns', 'faq_add_acf_columns' );

function faq_custom_column ( $column, $post_id ) {
   switch ( $column ) {     
     case 'faq_question':
     case 'faq_answer':
       echo wp_trim_words( get_field( $column, $post_id ), 20 );
       break;      
   }
}
add_action ( 'manage_cpt_faq_posts_custom_column', 'faq_custom_column', 10, 2 );


//*****************************************************************************************************
//add taxonomy filter(s) to admin list
function faq_taxonomy_filters() {  
    global $typenow;
  
    // an array of all the taxonomies you want to display. Use the taxonomy name or slug - each item gets its own select box.  
    $taxonomies = array('faq_categories');  
  
    // use the custom post type here  
    if( $typenow == 'cpt_faq' ){  
  
        foreach ($taxonomies as $tax_slug) {  
            $tax_obj = get_taxonomy($tax_slug);  
            $tax_name = $tax_obj->labels->name;  
            $terms = get_terms($tax_slug);  
            if(count($terms) > 0) {  
                echo '<select name='.$tax_slug.' id="'.$tax_slug.'" class="postform">';  
                echo '<option value="">Show All '.$tax_name.'</option>';  
                foreach ($terms as $term) {  
                    echo '<option value="'.$term->slug.'"  '. ( ( isset( $_GET[$tax_slug] ) && $_GET[$tax_slug] == $term->slug ) ? ' selected="selected"' : '' ).'>' . $term->name .' (' . $term->count .')</option>';  
                }  
                echo "</select>";  
            }  
        }  
    }  
}  
add_action( 'restrict_manage_posts', 'faq_taxonomy_filters' );  

//*****************************************************************************************************
//Order public archive page 
add_action( 'pre_get_posts', 'faq_archive_orderby'); 
function faq_archive_orderby($query){
    if( is_archive() && $query->is_main_query() && is_post_type_archive('cpt_faq') ):          
          $query->set( 'orderby', 'menu_order' );
          $query->set( 'order', 'ASC' );  
    endif;    
};
