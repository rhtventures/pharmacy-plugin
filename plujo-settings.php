<?php

/* Pharmacy Profile Settings Page */
class plujo_pharmacy_config_page {
  
	public function __construct() {
    
      $this->plujo_pages = array(
                  array('name'=>'Profile',
                        'default'=>'yes',
                        'slug'=>'pharmacy-profile'
                        ),
                  array('name'=>'Social Media',
                        'default'=>'no',
                        'slug'=>'pharmacy-social'
                        )        
              );

      $this->plujo_global = array( 'settings_title'=>'Pharmacy Config');
    
      add_action( 'admin_init', array( $this, 'plujo_setup_pages' ) ); 
      add_action( 'admin_menu', array( $this, 'plujo_create_settings' ) );
		                
      add_action( 'admin_init', array( $this, 'plujo_setup_sections' ) );
      add_action( 'admin_init', array( $this, 'plujo_setup_fields' ) );
    
	}
  
  
  public function plujo_setup_pages(){    
    
    foreach( $this->plujo_pages as $page ){
      
      if( isset(  $_GET['page'] ) && $_GET['page'] == $page['slug'] ){
        $this->plujo_global['page_current'] = $page;
        
      }      
      
      $fields_file = dirname(__FILE__).'/plujo-settings-'.$page['slug'].'.php';
      if( file_exists( $fields_file )){
          $settings_page = $page['slug'];
          include_once( $fields_file );
      }
      
    }
    
 }
  
	public function plujo_create_settings() {
      
      foreach( $this->plujo_pages as $page ){
          $page_title = $page['name'];
          $menu_title = ( $page['default'] == 'yes' )? $this->plujo_global['settings_title'] : '';
          $capability = 'manage_options';
          $slug = $page['slug'];
          $callback = array($this, 'plujo_settings_content');
          add_options_page( $page_title, $menu_title, $capability, $slug, $callback );
      }
	}  
  
	public function plujo_settings_content() { 
  

  ?>
		<div class="wrap">
			<h1>Pharmacy Configuration</h1>
      <?php ?> 
            
      <h2 class="nav-tab-wrapper">
          <?php 
          foreach( $this->plujo_pages as $page ){
            echo '<a href="?page='.$page['slug'].'" class="nav-tab '.(( isset( $this->plujo_global['page_current']['slug'] ) && $this->plujo_global['page_current']['slug'] == $page['slug'] )? 'nav-tab-active' : '' ).'">'.$page['name'].'</a>';
          }
          ?>   
      </h2>
      
			<form method="POST" action="options.php">
				<?php
              settings_fields( $this->plujo_global['page_current']['slug'] );
                                                                     
              echo do_settings_sections( $this->plujo_global['page_current']['slug'] );  
            
              submit_button();
				?>
			</form>
		</div> <?php
	}

  
   
 	public function plujo_setup_sections() {
    
    foreach( $this->plujo_pages as $page ){      
        add_settings_section( $page['slug'], '', array(), $page['slug'] );      
    }    

  }  

	public function plujo_setup_fields( $fields_array ) {
    
    foreach( $this->plujo_global['fields_array'] as $key=>$settings_page ){
      
        foreach( $settings_page as $field ){
          add_settings_field( $field['id'], $field['label'], array( $this, 'plujo_field_callback' ), $key, $field['section'], $field );
          register_setting( $key, $field['id'] );
        }
      
      
    }    
    
	}
  
	public function plujo_field_callback( $field ) {
		$value = get_option( $field['id'] );
		switch ( $field['type'] ) {
				case 'select':
				case 'multiselect':
					if( ! empty ( $field['options'] ) && is_array( $field['options'] ) ) {
						$attr = '';
						$options = '';
						foreach( $field['options'] as $key => $label ) {
							$options.= sprintf('<option value="%s" %s>%s</option>',
								$key,
								selected($value[array_search($key, $value, true)], $key, false),
								$label
							);
						}
						if( $field['type'] === 'multiselect' ){
							$attr = ' multiple="multiple" ';
						}
						printf( '<select name="%1$s[]" id="%1$s" %2$s>%3$s</select>',
							$field['id'],
							$attr,
							$options
						);
					}
					break;
      case 'textarea':
          printf( '<textarea class="regular-text ltr" name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s">%4$s</textarea>',
            $field['id'],
            $field['type'],
            $field['placeholder'],
            $value
          );
          break;        
			default:
				printf( '<input class="regular-text ltr" name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s"/>',
					$field['id'],
					$field['type'],
					$field['placeholder'],
					$value
				);
		}
		//if( $desc = $field['desc'] ) {
			printf( '<p class="description">id: %s </p>', $field['id'] );
		//}
	}
}

new plujo_pharmacy_config_page();

//Add shortcode for outputting global  variables
function pharmacyProfile_shortcode( $params ){
  
     if( is_array( get_option( $params['field']  )  ) )
        return get_option( $params['field']  )[0];
     else
        return get_option( $params['field']  );   
 }
add_shortcode('pharmacyProfile', 'pharmacyProfile_shortcode');